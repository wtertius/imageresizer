package resize

import (
	"image"

	"github.com/nfnt/resize"
)

func Resize(img image.Image, width, height int) image.Image {
	algorithm := chooseAlgorithm(img, width, height)
	return resize.Resize(uint(width), uint(height), img, algorithm)
}

// Use MitchellNetravali for scaling up and Lanczos3 for scaling down
// The explanation is here:
// https://stackoverflow.com/questions/384991/what-is-the-best-image-downscaling-algorithm-quality-wise
func chooseAlgorithm(img image.Image, width, height int) resize.InterpolationFunction {
	algorithm := resize.Lanczos3
	if isImageSizeMoreThan(img, width, height) {
		algorithm = resize.MitchellNetravali
	}

	return algorithm
}

func isImageSizeMoreThan(img image.Image, width, height int) bool {
	bounds := img.Bounds()
	return (bounds.Max.X-bounds.Min.X)*(bounds.Max.Y-bounds.Min.Y) > width*height
}
