package resize

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"image"
	"image/color"
	_ "image/jpeg"

	"github.com/nfnt/resize"

	"imageresizer/web/test"
)

func TestResize(t *testing.T) {
	t.Run("isImageSizeMoreThan & chooseAlgorithm", func(t *testing.T) {
		t.Run("400x600 is less than 500x700. Use Lanczos3", func(t *testing.T) {
			img := ImageDummy{width: 400, height: 600}
			width := 500
			height := 700

			assert.False(t, isImageSizeMoreThan(img, width, height))
			assert.Equal(t, chooseAlgorithm(img, width, height), resize.Lanczos3)
		})
		t.Run("400x600 is more than 300x500. Use MitchellNetravali", func(t *testing.T) {
			img := ImageDummy{width: 400, height: 600}
			width := 300
			height := 500

			assert.True(t, isImageSizeMoreThan(img, width, height))
			assert.Equal(t, chooseAlgorithm(img, width, height), resize.MitchellNetravali)
		})
		t.Run("400x400 is more than 300x500. Use MitchellNetravali", func(t *testing.T) {
			img := ImageDummy{width: 400, height: 400}
			width := 300
			height := 500

			assert.True(t, isImageSizeMoreThan(img, width, height))
			assert.Equal(t, chooseAlgorithm(img, width, height), resize.MitchellNetravali)
		})
	})

	t.Run("Resize", func(t *testing.T) {
		img := test.ImageGet()
		t.Run("to 400x500", func(t *testing.T) {
			width := 400
			height := 500

			imgResized := Resize(img, width, height)
			assert.Equal(t, imgResized.Bounds().Min.X, 0)
			assert.Equal(t, imgResized.Bounds().Max.X, width)
			assert.Equal(t, imgResized.Bounds().Min.Y, 0)
			assert.Equal(t, imgResized.Bounds().Max.Y, height)
		})
		t.Run("to 900x800", func(t *testing.T) {
			width := 900
			height := 800

			imgResized := Resize(img, width, height)
			assert.Equal(t, imgResized.Bounds().Min.X, 0)
			assert.Equal(t, imgResized.Bounds().Max.X, width)
			assert.Equal(t, imgResized.Bounds().Min.Y, 0)
			assert.Equal(t, imgResized.Bounds().Max.Y, height)
		})
	})
}

type ImageDummy struct {
	width  int
	height int
}

func (ImageDummy) ColorModel() color.Model {
	return nil
}

func (img ImageDummy) Bounds() image.Rectangle {
	return image.Rectangle{
		Min: image.Point{X: 0, Y: 0},
		Max: image.Point{X: img.width, Y: img.height},
	}
}
func (ImageDummy) At(x, y int) color.Color {
	return nil
}
