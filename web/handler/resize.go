package handler

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"image"
	"image/jpeg"
	"net/http"

	"github.com/yvasiyarov/groupcache/lru"
	"imageresizer/web/lib/resize"
)

const imageTypeJPEG = "jpeg"
const requiredImageURL = "'url' param is required. Value should be correct url\n"
const requiredWidth = "'width' param is required. Value should be positive integer.\n"
const requiredHeight = "'height' param is required. Value should be positive integer.\n"
const cantDownloadImage = "can't get image by url"
const cantDecodeImage = "can't decode image"

func NewResize(cache *lru.CacheTTL) *Resize {
	return &Resize{
		cache: cache,
	}

}

type Resize struct {
	cache *lru.CacheTTL
}

type resultEntity struct {
	response []byte
	hashSum  string
}

type parameters struct {
	imageURL string
	width    int
	height   int
}

func (handler *Resize) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	params := &parameters{
		imageURL: parseURLFromRequest(req, "url", ""),
		width:    int(parseUintFromRequest(req, "width", 0)),
		height:   int(parseUintFromRequest(req, "height", 0)),
	}

	message := handler.checkParams(params)
	if message != "" {
		w.WriteHeader(http.StatusExpectationFailed)
		fmt.Fprint(w, message)
		return
	}

	cacheKey := handler.BuildCacheKey(params)

	result := handler.GetFromCache(cacheKey)
	if result != nil {
		handler.Write(w, req, result)
		return
	}

	response := handler.ServeImage(w, params)
	if response == nil {
		return
	}

	result = &resultEntity{
		response: response,
		hashSum:  takeHashSum(response),
	}

	handler.cache.Add(cacheKey, result)

	handler.Write(w, req, result)
	return
}

func (handler *Resize) checkParams(params *parameters) string {
	message := ""
	if params.imageURL == "" {
		message += requiredImageURL
	}
	if params.width < 1 {
		message += requiredWidth
	}
	if params.height < 1 {
		message += requiredHeight
	}

	return message
}

func (handler *Resize) BuildCacheKey(params *parameters) string {
	cacheKey := fmt.Sprintf("url=%s&width=%d&height=%d", params.imageURL, params.width, params.height)
	return cacheKey
}

func (handler *Resize) GetFromCache(requestKey string) *resultEntity {
	value, ok := handler.cache.Get(requestKey)
	if ok && value != nil {
		result, ok := value.(*resultEntity)
		if ok && result != nil {
			return result
		}
	}

	return nil
}

func (handler *Resize) ServeImage(w http.ResponseWriter, params *parameters) []byte {
	imageResponse, err := http.Get(params.imageURL)
	if err != nil || imageResponse.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		fmt.Fprint(w, cantDownloadImage)
		return nil
	}

	img, imageType, err := image.Decode(imageResponse.Body)
	if err != nil || imageType != imageTypeJPEG {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		fmt.Fprint(w, cantDecodeImage)
		return nil
	}

	img = resize.Resize(img, params.width, params.height)

	var imageEncoded bytes.Buffer
	jpeg.Encode(&imageEncoded, img, nil)

	response := imageEncoded.Bytes()
	return response
}

func takeHashSum(b []byte) string {
	hash := md5.New()
	hash.Write(b)
	return fmt.Sprintf("%x", hash.Sum(nil))
}

func (handler *Resize) Write(w http.ResponseWriter, req *http.Request, result *resultEntity) {
	if req.Header.Get("If-None-Match") == result.hashSum {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	header := w.Header()
	header.Set("ETag", result.hashSum)
	header.Set("Cache-Control", "public, max-age=3600")

	w.Write(result.response)
}
