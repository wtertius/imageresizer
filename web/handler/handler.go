package handler

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func parseUintFromRequest(r *http.Request, name string, value uint64) uint64 {
	return parseUintFromString(r.URL.Query().Get(name), value)
}

func parseUintFromString(raw string, value uint64) uint64 {
	if number, err := strconv.ParseUint(raw, 10, 0); err == nil && number > 0 {
		value = number
	}

	return value
}

func parseIntFromRequest(r *http.Request, name string, value int64) int64 {
	return parseIntFromString(r.URL.Query().Get(name), value)
}

func parseIntFromString(raw string, value int64) int64 {
	if number, err := strconv.ParseInt(raw, 10, 0); err == nil {
		value = number
	}

	return value
}

func parseURLFromRequest(r *http.Request, name string, value string) string {
	return parseURLFromString(r.URL.Query().Get(name), value)
}

func parseURLFromString(raw string, value string) string {
	valueString := parseStringFromString(raw, value)

	urlParsed, err := url.Parse(valueString)
	if err != nil || urlParsed.Scheme == "" || urlParsed.Host == "" {
		return value
	}

	return urlParsed.String()
}

func parseStringFromRequest(r *http.Request, name string, value string) string {
	return parseStringFromString(r.URL.Query().Get(name), value)
}

func parseStringFromString(raw string, value string) string {
	if raw != "" {
		value = strings.TrimSpace(raw)
	}

	return value
}
