package handler

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"image"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/yvasiyarov/groupcache/lru"

	"imageresizer/web/test"
)

func TestResize(t *testing.T) {
	imageServer := test.ImageServer()
	defer imageServer.Close()

	serverURL := "http://imageresizer.com/resize/"
	imageURL := imageServer.URL
	width := 400
	height := 500
	etag := "2a4bc1a6e939badf0919fd83ca45fa57"

	cacheTimeout := time.Second / 10
	cache := lru.NewTTL(2000, cacheTimeout)
	handlerResize := NewResize(cache)

	t.Run("Methods", func(t *testing.T) {
		serverURL := test.BuildURL(serverURL, imageURL, width, height)

		t.Run("GET is allowed", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusOK, recorder.Code)
		})
		t.Run("HEAD is prohibited", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodHead, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusMethodNotAllowed, recorder.Code)
		})
		t.Run("POST is prohibited", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodPost, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusMethodNotAllowed, recorder.Code)
		})
		t.Run("PUT is prohibited", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodPut, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusMethodNotAllowed, recorder.Code)
		})
		t.Run("DELETE is prohibited", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodDelete, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusMethodNotAllowed, recorder.Code)
		})
	})

	t.Run("Requires params", func(t *testing.T) {
		t.Run("url", func(t *testing.T) {
			serverURL := test.BuildURL(serverURL, "", width, height)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusExpectationFailed, recorder.Code)
			assert.Equal(t, requiredImageURL, test.ReadRecorderBody(recorder))
		})
		t.Run("width", func(t *testing.T) {
			serverURL := test.BuildURL(serverURL, imageURL, 0, height)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusExpectationFailed, recorder.Code)
			assert.Equal(t, requiredWidth, test.ReadRecorderBody(recorder))
		})
		t.Run("height", func(t *testing.T) {
			serverURL := test.BuildURL(serverURL, imageURL, width, 0)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusExpectationFailed, recorder.Code)
			assert.Equal(t, requiredHeight, test.ReadRecorderBody(recorder))
		})
	})

	t.Run("Requires correct params", func(t *testing.T) {
		t.Run("url", func(t *testing.T) {
			serverURL := test.BuildURL(serverURL, "no such url", width, height)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusExpectationFailed, recorder.Code)
			assert.Equal(t, requiredImageURL, test.ReadRecorderBody(recorder))
		})
		t.Run("width", func(t *testing.T) {
			serverURL := test.BuildURL(serverURL, imageURL, -1, height)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusExpectationFailed, recorder.Code)
			assert.Equal(t, requiredWidth, test.ReadRecorderBody(recorder))
		})
		t.Run("height", func(t *testing.T) {
			serverURL := test.BuildURL(serverURL, imageURL, width, -1)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusExpectationFailed, recorder.Code)
			assert.Equal(t, requiredHeight, test.ReadRecorderBody(recorder))
		})
	})

	t.Run("imageURL response", func(t *testing.T) {
		t.Run("shouldn't be error", func(t *testing.T) {
			imageURL := imageServer.URL + "/error/"
			serverURL := test.BuildURL(serverURL, imageURL, width, height)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusUnsupportedMediaType, recorder.Code)
			assert.Equal(t, cantDownloadImage, test.ReadRecorderBody(recorder))
		})
		t.Run("shouldn't be other type", func(t *testing.T) {
			imageURL := imageServer.URL + "/string/"
			serverURL := test.BuildURL(serverURL, imageURL, width, height)

			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusUnsupportedMediaType, recorder.Code)
			assert.Equal(t, cantDecodeImage, test.ReadRecorderBody(recorder))
		})
	})

	t.Run("handler", func(t *testing.T) {
		serverURL := test.BuildURL(serverURL, imageURL, width, height)

		t.Run("responds with", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)

			t.Run("status OK", func(t *testing.T) {
				assert.Equal(t, http.StatusOK, recorder.Code)
			})

			img, imageType, err := image.Decode(recorder.Body)
			t.Run("jpeg image", func(t *testing.T) {
				assert.Nil(t, err)
				assert.Equal(t, imageTypeJPEG, imageType)
				assert.NotNil(t, img)
			})
			t.Run("resized", func(t *testing.T) {
				bounds := img.Bounds()
				assert.Equal(t, width, bounds.Max.X-bounds.Min.X)
				assert.Equal(t, height, bounds.Max.Y-bounds.Min.Y)
			})

			response := recorder.Result()
			t.Run("jpeg content type header", func(t *testing.T) {
				assert.Equal(t, "image/jpeg", response.Header.Get("Content-Type"))
			})
		})
		t.Run("sets cache headers", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)

			response := recorder.Result()
			t.Run("Cache-Control and ETag", func(t *testing.T) {
				assert.Equal(t, "public, max-age=3600", response.Header.Get("Cache-Control"))
				assert.Equal(t, etag, response.Header.Get("ETag"))
			})
		})
		t.Run("returns StatusNotModified for same If-None-Match", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)
			request.Header.Set("If-None-Match", etag)

			handlerResize.ServeHTTP(recorder, request)

			assert.Equal(t, http.StatusNotModified, recorder.Code)
		})
		t.Run("caches response", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)

			requestCount := imageServer.RequestCount()
			handlerResize.ServeHTTP(recorder, request)

			assert.Equal(t, requestCount, imageServer.RequestCount())
		})
		t.Run("caches response not more than for timeout", func(t *testing.T) {
			recorder := httptest.NewRecorder()
			request := httptest.NewRequest(http.MethodGet, serverURL, nil)

			handlerResize.ServeHTTP(recorder, request)

			time.Sleep(cacheTimeout + time.Millisecond)
			requestCount := imageServer.RequestCount()
			handlerResize.ServeHTTP(recorder, request)

			assert.Equal(t, requestCount+1, imageServer.RequestCount())
		})
	})
}
