package test

import (
	"fmt"
	"image"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"sync"
)

var imagePath = "/root/go/src/imageresizer/web/lib/resize/test_data/tertius.jpeg"

func ImageGet() image.Image {
	imageReader := ImageReadFromFile(imagePath)
	img := ImageDecode(imageReader)

	return img
}

func ImageReadFromFile(path string) io.Reader {
	imageReader, err := os.Open(path)
	if err != nil {
		log.Fatalf("Can't open image from path '%s': %s", imagePath, err)
	}

	return imageReader
}

func ImageDecode(imageReader io.Reader) image.Image {
	img, _, err := image.Decode(imageReader)
	if err != nil {
		log.Fatal(err)
	}

	return img
}

func BuildURL(serverURL, imageURL string, width, height int) string {
	u, err := url.Parse(serverURL)
	if err != nil {
		log.Fatal(err)
	}

	q := u.Query()

	if imageURL != "" {
		q.Set("url", imageURL)
	}
	if width != 0 {
		q.Set("width", strconv.Itoa(width))
	}
	if height != 0 {
		q.Set("height", strconv.Itoa(height))
	}

	u.RawQuery = q.Encode()

	return u.String()
}

func ReadRecorderBody(w *httptest.ResponseRecorder) string {
	response := w.Result()
	body, _ := ioutil.ReadAll(response.Body)

	return string(body)
}

type requestCounter struct {
	count int
	sync.RWMutex
}

func (c *requestCounter) Inc() {
	c.Lock()
	defer c.Unlock()

	c.count++
}

type server struct {
	counter *requestCounter
	*httptest.Server
}

func (s *server) RequestCount() int {
	s.counter.RLock()
	defer s.counter.RUnlock()

	return s.counter.count
}

func NewServer() *server {
	mux := http.NewServeMux()
	mux.HandleFunc("/error/", handleWriteError)
	mux.HandleFunc("/string/", handleWriteString)
	mux.HandleFunc("/", handleWriteJPEG)

	counter := requestCounter{count: 0}

	testServer := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			counter.Inc()
			mux.ServeHTTP(w, req)
		}),
	)
	imageServer := &server{Server: testServer, counter: &counter}

	return imageServer
}

func ImageServer() *server {
	return NewServer()
}

func handleWriteError(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}

func handleWriteString(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)

	fmt.Fprint(w, "string")
}

func handleWriteJPEG(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)

	imageReader := ImageReadFromFile(imagePath)
	io.Copy(w, imageReader)
}
