package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/yvasiyarov/groupcache/lru"
	"imageresizer/web/handler"
)

const cacheSize = 2000

func main() {
	port := ":8080"
	log.Printf("\n\nAPI started at port '%s' with pid '%d'\n\n", port, os.Getpid())

	cache := lru.NewTTL(cacheSize, time.Hour)

	http.Handle("/", handler.NewResize(cache))

	log.Fatal(http.ListenAndServe(port, nil))
}
