CONTAINER=imageresizer
EXEC=docker-compose exec ${CONTAINER}

DIR=/root/go/src/imageresizer
BIN=${DIR}/bin/web

MAIN=${DIR}/web/main.go
CMD_BUILD=go build -o ${BIN} -i ${MAIN}
GET_PID = docker-compose logs ${CONTAINER} | grep pid | tail -n 1 | sed "s/.*with pid '\([0-9][0-9]*\)'/\1/"

bash: cli
cli:
	${EXEC} bash
run:
	${EXEC} ${CMD_BUILD}
	${EXEC} ${BIN}

build:
	${EXEC} CompileDaemon -build='${CMD_BUILD}' -command=${BIN}
logs:
	docker-compose logs --tail="15" -f ${CONTAINER}
debug:
	${EXEC} dlv attach $$( ${GET_PID} )

DC_BIN = docker-compose
DC = ${DC_BIN}

up:
	$(DC) up -d
down:
	$(DC) down

readme:
	@pandoc README.md | lynx -stdin -dump
