# Задача

Веб-сервис для ресайза картинок.
Принимает параметры запроса:

  * url - адрес картинки
  * width - желаемая ширина
  * height - желаемая высота

Результат: картинка в новом размере.

### Допущения:

  * реализовать только для формата JPEG
  * все параметры в запросе обязательные

### Требования:

  * тесты
  * кеширование результата на 1 час
  * заголовки для кеширования в браузере


# Решение

### Запуск
```
# docker-compose is required

make up
http://localhost:8080/?url=https://static.life.ru/posts/2017/12/1066864/29b97650a203d77b462c0b16e8be61f5__980x.jpg&width=1300&height=800

```

### Код и тесты в папке web

```
$ tree web/
web/
├── handler
│   ├── handler.go
│   ├── resize.go
│   └── resize_test.go
├── lib
│   └── resize
│       ├── resize.go
│       ├── resize_test.go
│       └── test_data
│           └── tertius.jpeg
├── main.go
└── test
    └── test.go
```

### Разработано по методологии TDD
```
# go test -coverprofile=cover.out ./...
?       imageresizer/web        [no test files]
ok      imageresizer/web/handler        0.254s  coverage: 93.8% of statements
ok      imageresizer/web/lib/resize     0.100s  coverage: 100.0% of statements
?       imageresizer/web/test   [no test files]
```

### Возможные улучшения

  * Использовать более быстрые утилиты для обработки изображений, например vipsthumbnail
  * Ограничить количество go-рутин, обрабатывающих изображения. Количество го-рутин выбирать динамически методом градиентного спуска
  * Отмена обработки запроса по таймауту
